const express = require('express')
const app = express()
const port = 3000
const QRCode = require("qrcode")
const {join} = require("path");
const {createPDF} = require("./pdf/pdf");


app.use(express.static('public'))

app.get('/',async  (req, res) => {
    await createPDF()
    res.send('Hello World!')
})
app.get("/pdf",(req,res)=>{
    res.sendFile(join(__dirname+'/pdf/pdf.html'));
})

app.listen(port, () => {
    console.log(`Example app listening on port ${port}`)
})