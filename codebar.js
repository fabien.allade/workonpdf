import QRCode from "qrcode";
import barcode from "barcode";
import * as path from "path";

function makeQRCode(data){
    QRCode.toString('data', {
        errorCorrectionLevel: 'H',
        type: 'svg'
    }, function(err, data) {
        if (err) throw err;
        console.log(data);
    });
}
function makeCodeBar(data,name){
    const code39 = barcode('code39', {
        data: data,
        width: 400,
        height: 100,
    });
    const outfile = path.join(__dirname, 'imgs', 'mycode.png')
}
export default {makeQRCode, makeCodeBar}