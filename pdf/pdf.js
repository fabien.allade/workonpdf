const  puppeteer = require("puppeteer");
const path = require("path") ;
const handlebars = require("handlebars");
const fs = require("fs")


const minimal_args = [
    '--autoplay-policy=user-gesture-required',
    '--disable-background-networking',
    '--disable-background-timer-throttling',
    '--disable-backgrounding-occluded-windows',
    '--disable-breakpad',
    '--disable-client-side-phishing-detection',
    '--disable-component-update',
    '--disable-default-apps',
    '--disable-dev-shm-usage',
    '--disable-domain-reliability',
    '--disable-extensions',
    '--disable-features=AudioServiceOutOfProcess',
    '--disable-hang-monitor',
    '--disable-ipc-flooding-protection',
    '--disable-notifications',
    '--disable-offer-store-unmasked-wallet-cards',
    '--disable-popup-blocking',
    '--disable-print-preview',
    '--disable-prompt-on-repost',
    '--disable-renderer-backgrounding',
    '--disable-setuid-sandbox',
    '--disable-speech-api',
    '--disable-sync',
    '--hide-scrollbars',
    '--ignore-gpu-blacklist',
    '--metrics-recording-only',
    '--mute-audio',
    '--no-default-browser-check',
    '--no-first-run',
    '--no-pings',
    '--no-sandbox',
    '--no-zygote',
    '--password-store=basic',
    '--use-gl=swiftshader',
    '--use-mock-keychain',
    '--use-gl=egl',
    '--disable-dev-shm-usage'
];
 async function createPDF(data){
     console.time("fabien")
    const templateHtml = fs.readFileSync(path.join(__dirname, '/pdf.html'), 'utf8');
    const template = handlebars.compile(templateHtml);
    const html = encodeURIComponent(template(data));

    let milis = new Date();
    milis = milis.getTime();

    const pdfPath = path.join('pdf/upload/', `${milis}.pdf`);

    const options = {
        // width: '1230px',
        headerTemplate: "<p></p>",
        footerTemplate: "<p></p>",
        displayHeaderFooter: false,
        margin: {
            top: "10px",
            bottom: "30px"
        },
        printBackground: true,
        omitBackground:true,
        path: pdfPath
    }

    const browser = await puppeteer.launch({
        headless: "new",
    });

    const page = await browser.newPage();
    await page.goto(`data:text/html;charset=UTF-8,${html}`, {
        waitUntil: 'networkidle2'
    });

    await page.pdf(options);
     await browser.close();
     console.timeEnd("fabien")
}

module.exports.createPDF = createPDF